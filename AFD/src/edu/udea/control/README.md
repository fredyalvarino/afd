[img_union]: ../assets/AFD-icon-420.png "Autómatas Finitos Deterministicos"


![Descripcion del trabajo pendiente][img_union] Un texto metido a ver que pasa

# Descripción del laboratorio
Construya programa de computador que acepte como entrada dos autómatas finitos determinísticos con las siguientes condiciones:

Desplegar un menú en el cual haya dos opciones: 
1.  Construir la unión de los dos autómatas finitos entrados
2.  Construir la intersección de los dos autómatas finitos entrados.


Cualquiera que sea la opción elegida por el usuario, el autómata finito resultante debe
1.  Ser mínimo
2.  El usuario deberá poder entrar hileras para comprobar el correcto funcionamiento del autómata creado.

Su programa debe ser tal que el usuario pueda visualizar simultáneamente los dos autómatas entrados y el construido por el programa.  
Fecha de entrega: **mayo 31 del 2016** 
> Universidad de Antioquia&trade; 

------------------------------------------------------------

# Realizado

*   Ingreso de simbolos de entrada
*   Ingreso de estados
*   Ingreso del automata finito determinista (`AFD`) 
- Unión de dos (`AFD`)
- Intersección de dos (`AFD`)
- Ingresar hileras para verificar el automata (`AFD`)

> Selección automática por medio de `combobox` 

[Código fuente](https://fredyalvarino@bitbucket.org/fredyalvarino/afd.git "Clonar proyecto de Bitbucket") 

&copy; 2016 Universidad de Antioquia