package edu.udea.control;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import edu.udea.control.Util.IconsEnum;

/**
 * Panel de validacion y verificarion de tramas y de estados
 * 
 * @author Fredy Alvarino
 *
 */
@SuppressWarnings("serial")
public class PanelTest extends JPanel {

	private JComboBox<String> cbEstado;
	private JComboBox<String> cbPanel;
	private List<PanelAF> paneles = new ArrayList<PanelAF>();
	private JTextPane tpBuffer;

	/**
	 * Creacion de los componentes del panel
	 */
	public PanelTest() {
		setBorder(new TitledBorder(null, "Validar trama y estados conocidos", TitledBorder.LEADING, TitledBorder.TOP,
				null, null));
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);

		cbPanel = new JComboBox<String>();
		cbPanel.setToolTipText("Seleccionar Automata Finito");
		cbPanel.addItem("AF (Res)");
		cbPanel.addItem("AF (1)");
		cbPanel.addItem("AF (2)");
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		if (cbPanel.getItemCount() > 0) {
			cbPanel.setSelectedIndex(0);
		}

		JButton btnVerificar = new JButton(Util.getIcon(IconsEnum.CHECK));
		btnVerificar.setToolTipText("Verificar Trama");

		JButton btnLimpiar = new JButton(Util.getIcon(IconsEnum.ERASER));
		btnLimpiar.setToolTipText("Restablecer valores predeterminados");

		cbEstado = new JComboBox<String>();
		cbEstado.setToolTipText("Seleccionar Estado Inicial");
		if (cbEstado.getItemCount() > 0) {
			cbEstado.setSelectedIndex(0);
		}

		final JTextField tfTrama = new JTextField();
		tfTrama.setToolTipText("Ingresar Trama de prueba");
		tfTrama.setColumns(17);

		panel.add(cbPanel);
		panel.add(cbEstado);
		panel.add(tfTrama);
		panel.add(btnVerificar);
		panel.add(btnLimpiar);

		tpBuffer = new JTextPane();
		JScrollPane sp = new JScrollPane(tpBuffer);
		tpBuffer.setEditable(false);

		add(sp);

		cbPanel.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					setEstados();
				}
			}
		});
		tfTrama.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkThread(tpBuffer, tfTrama);
			}
		});
		btnVerificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkThread(tpBuffer, tfTrama);
			}
		});
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tpBuffer.setText(null);
				tfTrama.setText(null);
				setEstados();
			}
		});
	}

	/**
	 * Adiciona y actualiza las referencias a los paneles que representan los
	 * automatas finitos
	 * 
	 * @param pRES
	 * @param p1
	 * @param p2
	 */
	public void setConfig(PanelAF pRES, PanelAF p1, PanelAF p2) {
		paneles.clear();
		paneles.add(pRES);
		paneles.add(p1);
		paneles.add(p2);
	}

	/**
	 * Obtiene la informacion de un automata finito seleccionado
	 * 
	 * @return
	 */
	private Map<String, Vector<String>> getData() {
		int index = cbPanel.getSelectedIndex();
		if (index > paneles.size() - 1) {
			Util.printErr("No se encontraron datos");
			return null;
		}
		Map<String, Vector<String>> map = Util.toMap(paneles.get(index));
		return map;
	}

	/**
	 * Obtiene los simbolos de entrada para el automata seleccionado
	 * 
	 * @return
	 */
	private Vector<String> getHeaders() {
		int index = cbPanel.getSelectedIndex();
		if (index > paneles.size() - 1) {
			Util.printErr("No se encontraron datos");
			return null;
		}
		Enumeration<TableColumn> columns = paneles.get(index).getTable().getColumnModel().getColumns();
		Vector<String> vector = new Vector<String>();
		while (columns.hasMoreElements()) {
			Object headerValue = columns.nextElement().getHeaderValue();
			if (headerValue instanceof javax.swing.JLabel) {
				javax.swing.JLabel new_name = (javax.swing.JLabel) headerValue;
				headerValue = new_name.getToolTipText();
			}
			vector.addElement(headerValue.toString());
		}
		return vector;
	}

	/**
	 * Actualiza los estados segun el automata seleccionado.
	 */
	public void setEstados() {
		// Se restablecen el selector de estados
		int index = cbPanel.getSelectedIndex();
		if (index > paneles.size() - 1) {
			System.out.println("No se encontraron datos");
			return;
		}
		PanelAF panelAF = paneles.get(index);
		List<String> list = Util.getSimbolos(panelAF.getTable());
		cbEstado.removeAllItems();
		for (String item : list) {
			cbEstado.addItem(item);
		}
		if (cbEstado.getItemCount() > 0) {
			cbEstado.setSelectedIndex(0);
		}
	}

	/**
	 * Realiza la validacion y la verificacio de la trama entregada segun el
	 * automata entregado
	 * 
	 * @param tpBuffer
	 * @param tfTrama
	 */
	private void checkThread(JTextPane tpBuffer, JTextField tfTrama) {
		Object item = cbEstado.getSelectedItem();
		if (item == null) {
			Util.showError(this, "No hay elementos para procesar");
			return;
		}
		String inicio = item.toString();
		Vector<String> headers = getHeaders();
		Map<String, Vector<String>> af = getData();
		Util.verificarTrama(tpBuffer, tfTrama, inicio, headers, af);
	}

	/**
	 * Escribe un mensaje con el formato estandar de linea en el componente
	 * {@link JTestPane}
	 * 
	 * @param msg
	 *            mensaje a escribir
	 */
	public void showMsg(String msg) {
		StringBuffer sb = new StringBuffer();
		sb.append(tpBuffer.getText());
		Util.addLineDate(sb, msg);
		tpBuffer.setText(sb.toString());
	}

}
