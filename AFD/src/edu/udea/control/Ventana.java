package edu.udea.control;

import static edu.udea.control.Util.VACIO;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import edu.udea.control.Util.IconsEnum;

/**
 * Clase ventana, la cual contiene toda la aplicacion
 * @author Fredy Alvarino
 */
public class Ventana {

	private static final String TITLE_SIMBOLO_ENTRADA = "Simbolos";
	private static final String[] SIMBOLOS_INICIO_ENCABEZADO = new String[] { "" };
	public static final Object[][] DATOS_BASICOS = new Object[][] { { "0" }, { "1" }};
	public static final Object[][] OTROS_DATOS = new Object[][] { { "a" }, { "b" }, { "c" }, { "d" }, { "e" } };
	public static final Object[][] SIMBOLOS_INICIO_DATOS_BASICOS = DATOS_BASICOS;
	private JFrame ventana;
	private JTable tbEstados;
	private JPanel panelSimbolosEntrada;
	private JPanel panel;
	private JButton btnAdd;
	private JButton btnDel;
	private JScrollPane scrollPane;

	/**
	 * Despliega la aplicacion.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					Ventana window = new Ventana();
					window.ventana.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Inicializa la aplicacion.
	 */
	public Ventana() {
		initialize();
	}

	/**
	 * Inicializa los contenidos de la Ventana.
	 */
	private void initialize() {
		ventana = new JFrame();
		ventana.setResizable(false);
		ventana.setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/edu/udea/assets/intersection-32.png")));
		ventana.setTitle("ODAF - (Operador de Automatas Finitos)");
		ventana.setBounds(100, 100, 962, 617);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.getContentPane().setLayout(null);

		construirPanelSimbolos(ventana);

		final PanelAF panelAF_1 = new PanelAF("Aut\u00F3mata Finito (1)");
		panelAF_1.setBounds(109, 11, 336, 255);
		ventana.getContentPane().add(panelAF_1);

		final PanelAF panelAF_2 = new PanelAF("Aut\u00F3mata Finito (2)");
		panelAF_2.setBounds(109, 277, 336, 255);
		ventana.getContentPane().add(panelAF_2);

		final PanelAF panelAF_RES = new PanelAF("Aut\u00F3mata Finito (Respuesta)");
		panelAF_RES.setBounds(473, 11, 463, 334);
		panelAF_RES.setEditable(false);
		ventana.getContentPane().add(panelAF_RES);

		PanelAcciones pAcciones = new PanelAcciones("Acciones");
		pAcciones.setBounds(10, 187, 89, 320);
		pAcciones.setLayout(new GridLayout(0, 1, 0, 0));
		ventana.getContentPane().add(pAcciones);

		
		PanelTest pTesting = new PanelTest();
		pTesting.setBounds(473, 356, 463, 165);
		pTesting.setConfig(panelAF_RES, panelAF_1, panelAF_2);
		ventana.getContentPane().add(pTesting);
		
		pAcciones.configurar(tbEstados, panelAF_1, panelAF_2, panelAF_RES, pTesting);
	}

	/**
	 * Construye el panes para manejar los Simbolos de entrada
	 * @param frame
	 */
	private void construirPanelSimbolos(final JFrame frame) {
		panelSimbolosEntrada = new JPanel();
		panelSimbolosEntrada.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), TITLE_SIMBOLO_ENTRADA,
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelSimbolosEntrada.setBounds(10, 11, 84, 165);
		frame.getContentPane().add(panelSimbolosEntrada);
		panelSimbolosEntrada.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		panel = new JPanel();
		panelSimbolosEntrada.add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		btnAdd = new JButton(Util.getIcon(IconsEnum.ADD_GREEN));
		btnAdd.setToolTipText("Adicionar Simbolo");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Util.adicionarEntrada(tbEstados);
			}
		});
		panel.add(btnAdd);

		btnDel = new JButton(VACIO);
		btnDel.setToolTipText("Eliminar simbolos seleccionados");
		btnDel.setIcon(Util.getIcon(IconsEnum.REM));
		btnDel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Util.removerFila(tbEstados);
			}
		});
		panel.add(btnDel);

		tbEstados = new JTable() {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false;
			}

			public Dimension getPreferredScrollableViewportSize() {
				return new Dimension(50, 70);
			}
		};
		tbEstados.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tbEstados.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		tbEstados.setModel(new DefaultTableModel(SIMBOLOS_INICIO_DATOS_BASICOS, SIMBOLOS_INICIO_ENCABEZADO));
		tbEstados.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tbEstados.setTableHeader(null);
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		TableColumn column = tbEstados.getColumnModel().getColumn(0);
		column.setCellRenderer(centerRenderer);
		column.setMaxWidth(50);
		scrollPane = new JScrollPane(tbEstados, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelSimbolosEntrada.add(scrollPane);
	}
}
