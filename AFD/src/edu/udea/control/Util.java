package edu.udea.control;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 * Realiza las diferentes operaciones generales de la aplicaci&oacute;n
 * 
 * @author Fredy Alvarino
 *
 */
@SuppressWarnings("unchecked")
public class Util {

	public static final String VACIO = "";
	private static final String TITLE_ERROR = "Error";
	private static final List<Integer> listaEstados = new ArrayList<Integer>();
	public static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	/**
	 * Adiciona un nuevo simbolo de entrada
	 * 
	 * @param table
	 */
	public static void adicionarEntrada(JTable table) {
		// addItem("Simbolo de entrada", false, false, table);
		String simbolo = null;
		simbolo = getInput(table, "Simbolo de entrada");
		if (VACIO.equals(simbolo)) {
			showError(table, String.format("El valor \"%s\" no es permitido", simbolo));
			return;
		}

		Set<String> dataSet = new TreeSet<String>();
		for (char c : simbolo.toCharArray()) {
			dataSet.add(String.valueOf(c));
		}
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		StringBuffer buffer = new StringBuffer();
		for (String value : dataSet) {
			// Verificamos si ya se encuentra dentro de los simbolos de entrada
			if (" ".equals(value)) {
				value = "\" \"";
			}
			boolean se_encuentra = false;
			for (Iterator<Vector<String>> iterator = model.getDataVector().iterator(); iterator.hasNext();) {
				Vector<String> dato = iterator.next();
				if (se_encuentra = value.equals(dato.firstElement())) {
					buffer.append(String.format("� El valor \"%s\" ya se encuentra inscrito\n", value));
					break;
				}
			}
			if (!se_encuentra) {
				model.addRow(new Vector<String>(Arrays.asList(value)));
			}
		}
		if (buffer.length() > 0) {
			showError(table, buffer.toString());
		}
	}

	/**
	 * Adiciona un nuevo estado a la tabla seleccionada Realiza la operacion
	 * respectiva para adicionar una nueva fila a la tabla entregada
	 * 
	 * @param table
	 */
	public static void addEstado(JTable... tables) {
		String msgGetDato = "Estado";
		boolean is_upper = true;
		String simbolo = null;

		simbolo = getInput(tables[0], msgGetDato);
		if (VACIO.equals(simbolo)) {
			showError(tables[0], String.format("El valor \"%s\" no es permitido", simbolo));
			return;
		}
		if (is_upper) {
			simbolo = simbolo.toUpperCase();
		}

		for (JTable table : tables) {
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			// Verificamos si ya se encuentra dentro de los simbolos de entrada
			boolean se_encuentra = false;
			for (Iterator<Vector<String>> iterator = model.getDataVector().iterator(); iterator.hasNext();) {
				Vector<String> dato = iterator.next();
				if (se_encuentra = simbolo.equals(dato.firstElement())) {
					showError(table, String.format("El valor \"%s\" ya se encuentra inscrito", simbolo));
					break;
				}
			}
			if (!se_encuentra) {
				int cantidad = model.getColumnCount();
				Vector<String> vector = new Vector<String>(Arrays.asList(simbolo));
				for (int i = 1; i < cantidad; i++) {
					vector.add((i == (cantidad - 1) ? String.valueOf(false) : simbolo));
				}
				model.addRow(vector);
			}
		}
	}

	/**
	 * 
	 * @param panelAF
	 */
	public static void configurarAdicion(final PanelAF panelAF) {
		Util.addEstado(panelAF.getTable());
		Vector<String> estadosPosibles = Util.setDefaultCombo(panelAF.getTable());
		panelAF.setEstadoInical(estadosPosibles);
	}

	/**
	 * Establece el estilo para la primera columna de una tabla
	 * 
	 * @param table
	 */
	public static void setFirstCellStyle(JTable table) {
		table.getColumnModel().getColumn(0).setCellRenderer(new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (column == 0) {
					comp.setBackground(Color.GRAY);
					comp.setForeground(Color.WHITE);
					comp.setFont(comp.getFont().deriveFont(Font.BOLD));
					setHorizontalAlignment(JLabel.CENTER);
				}
				return comp;
			}
		});
	}

	/**
	 * Establece el render por defecto a los campos de entrada para los
	 * automatas finitos dados
	 * 
	 * @param tables
	 *            {@link JTable}
	 * @return
	 */
	private static Vector<String> setDefaultCombo(JTable... tables) {
		Vector<String> vector = null;
		int columnCount = tables[0].getColumnCount();
		if (columnCount <= 2) {
			return vector; // AFD Vacio
		}

		JComboBox<String> comboBox;
		for (JTable table : tables) {
			vector = new Vector<String>(getSimbolos(table));
			comboBox = new JComboBox<String>(vector);
			setFirstCellStyle(table);
			for (int i = 1; i < columnCount; i++) {
				TableColumn column = table.getColumnModel().getColumn(i);
				if (i == (columnCount - 1)) {
					column.setCellEditor(new DefaultCellEditor(new JCheckBox()));
				} else {
					column.setCellEditor(new DefaultCellEditor(comboBox));
				}
			}
		}
		return vector;
	}

	/**
	 * Muestra un mensaje en popup al usuario con el mensaje dado
	 * 
	 * @param comp
	 *            componente relativo para mostrar el mensaje
	 * @param msg
	 *            mensaje para mostrar al usuario
	 */
	public static void showMsg(Component comp, String msg) {
		JOptionPane.showMessageDialog(comp, msg);
	}

	/**
	 * Muestra un mensaje de error en popup al usuario con el mensaje dado
	 * 
	 * @param comp
	 *            componente relativo para mostrar el mensaje
	 * @param msg
	 *            mensaje para mostrar al usuario
	 */
	public static void showError(Component comp, String msg) {
		JOptionPane.showMessageDialog(comp, msg, TITLE_ERROR, JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Optiene un valor de la gui por medio de un mensaje en popup al usuario
	 * con el mensaje dado
	 * 
	 * @param comp
	 *            componente relativo para mostrar el mensaje
	 * @param msg
	 *            mensaje para mostrar al usuario
	 */
	public static String getInput(Component comp, String msg) {
		String elemento = JOptionPane.showInputDialog(comp, msg);
		if (elemento != null && !elemento.isEmpty()) {
			return elemento.trim();
		} else {
			return VACIO;
		}
	}

	/**
	 * Obtiene los valores de la primera fila en una lista
	 * 
	 * @param tabla
	 *            {@link JTable} de referencia
	 * @return {@link List} simbolos de entrada
	 */
	public static List<String> getSimbolos(JTable tabla) {
		List<String> datos = new ArrayList<String>();
		Vector<Vector<String>> dataVector = ((DefaultTableModel) tabla.getModel()).getDataVector();
		for (Iterator<Vector<String>> iterator = dataVector.iterator(); iterator.hasNext();) {
			String element = iterator.next().firstElement();
			if (element != null) {
				datos.add(element);
			}
		}
		return datos;
	}

	/**
	 * Establece los iconos en los encabezados de las tablas
	 * 
	 * @param table
	 */
	public static void setIconHeader(JTable table) {

		table.setPreferredScrollableViewportSize(table.getPreferredSize());
		TableColumnModel columnModel = table.getColumnModel();

		int columnCount = columnModel.getColumnCount();

		TableColumn column0 = columnModel.getColumn(0);
		TableColumn column1 = columnModel.getColumn(columnCount - 1);

		TableCellRenderer renderer = new TableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				return (JComponent) value;
			}
		};

		Border headerBorder = UIManager.getBorder("TableHeader.cellBorder");

		JLabel blueLabel = new JLabel("", getIcon(IconsEnum.ACS), JLabel.CENTER);
		blueLabel.setBorder(headerBorder);
		blueLabel.setToolTipText("Estado");
		JLabel redLabel = new JLabel("", getIcon(IconsEnum.SUCCESS), JLabel.CENTER);
		redLabel.setBorder(headerBorder);
		redLabel.setToolTipText("�Estado de aceptacion?");

		column0.setHeaderRenderer(renderer);
		column0.setHeaderValue(blueLabel);

		column1.setHeaderRenderer(renderer);
		column1.setHeaderValue(redLabel);
	}

	/**
	 * Obtiene un objeto ImageIcon desde la ruta predeterminada para las
	 * imagenes
	 * 
	 * @param access
	 *            cadena con el nombre y extension del archivo de imagen
	 * @return {@link ImageIcon}
	 */
	static ImageIcon getIcon(IconsEnum access) {
		try {
			return new ImageIcon(Util.class.getResource("/edu/udea/assets/" + access.valor));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Realiza la operacion de unir los automatas finitos 1 y 2 y el resultado
	 * lo renderiza en el panel de respuesta
	 * 
	 * @param p1
	 *            Automata (1)
	 * @param p2
	 *            Automata (2)
	 * @param pOut
	 *            Automata de respuesta
	 * @param unir
	 *            indica si debe unir o interceptar los AFND
	 * @return Map con los valores de estados equivalentes
	 */
	public static Map<String, String> unirAFD(PanelAF p1, PanelAF p2, PanelAF pOut, boolean unir) {
		Vector<Vector<String>> v = (Vector<Vector<String>>) p1.getDatos().clone();
		v.addAll((Vector<Vector<String>>) p2.getDatos().clone());

		String ei_1 = p1.getEstadoInicial(), ei_2 = p2.getEstadoInicial();
		Map<String, String> map = new TreeMap<String, String>();
		if (ei_1 != null && ei_2 != null && !ei_1.isEmpty() && !ei_2.isEmpty()) {
			Map<String, Vector<String>> afnd = new LinkedHashMap<String, Vector<String>>();
			for (Vector<String> vector : v) {
				afnd.put(vector.get(0), vector);
			}
			Map<String, Vector<String>> res;
			res = new LinkedHashMap<String, Vector<String>>();
			String SEP = "_";
			String key = ei_1 + SEP + ei_2;
			printErr("\nDatos \n" + v);
			unirAutomatas(afnd, res, key, SEP, unir);

			// Quitar estados equivalentes
			v = new Vector<Vector<String>>(res.values());
			quitarIguales(v, map);
		}
		// Render Panel Result
		pOut.setEditable(false);
		pOut.render(p1.getColumnas(), v);
		return map;
	}

	private static void unirAutomatas(Map<String, Vector<String>> afnd, Map<String, Vector<String>> union, String key,
			String SEP, boolean unir) {

		String[] keys = key.split(SEP);
		if (keys.length < 2 || union.containsKey(key)) {
			printErr(String.format("Clave %s invalida o la clave ya se encuentra en la union \n%s", key, union));
			return;
		}

		Vector<String> trans = new Vector<String>();
		Vector<String> t1 = afnd.get(keys[0]);
		Vector<String> t2 = afnd.get(keys[1]);
		for (int i = 0; t1 != null && t2 != null && i < t1.size(); i++) {
			String d1 = String.valueOf(t1.get(i));
			String d2 = String.valueOf(t2.get(i));
			if (i == t1.size() - 1) {
				boolean b1 = String.valueOf(true).equals(d1);
				boolean b2 = String.valueOf(true).equals(d2);
				printErr(d1 + " : " + d2);
				trans.addElement(String.valueOf(unir ? b1 || b2 : b1 && b2));
			} else {
				trans.addElement(d1 + SEP + d2);
			}
		}
		union.put(key, trans);
		// Se realiza la union de los nuevas transiciones encontradas
		for (int i = 1; i < trans.size() - 1; i++) {
			unirAutomatas(afnd, union, trans.get(i), SEP, unir);
		}
	}

	/**
	 * Realiza la operacion de Interceccion para los automatas finitos 1 y 2 y
	 * el resultado lo renderiza en el panel de respuesta
	 * 
	 * @param p1
	 *            Automata (1)
	 * @param p2
	 *            Automata (2)
	 * @param pOut
	 *            Automata de respuesta
	 * @return Map con los valores de estados equivalentes
	 */
	public static Map<String, String> intersectarAFD(PanelAF p1, PanelAF p2, PanelAF pOut) {
		Vector<Vector<String>> v = new Vector<Vector<String>>();
		Vector<Vector<String>> d1 = p1.getDatos();
		Vector<Vector<String>> d2 = p2.getDatos();

		for (Vector<String> v1 : d1) {
			for (Vector<String> v2 : d2) {
				if (isEquals(v1, v2)) {
					v.add(v1);
				}
			}
		}

		// Quitar estados equivalentes
		Map<String, String> map = new TreeMap<String, String>();
		quitarIguales(v, map);
		// Render Panel Result
		pOut.setEditable(false);
		pOut.render(p1.getColumnas(), v);
		return map;
	}

	/**
	 * Remueve del automata finito {m} los estados equivalentes
	 * 
	 * @param m
	 *            representa al automata finito
	 * @param map
	 *            {@link Map} con los estatos equivalentes
	 */
	private static void quitarIguales(Vector<Vector<String>> m, Map<String, String> map) {
		for (int i = m.size() - 1; i > 0; i--) {
			Vector<String> rowToCheck = m.get(i);
			for (int j = 0; j < i; j++) {
				Vector<String> vector = m.get(j);
				if (isEquals(rowToCheck, vector)) {
					String src = rowToCheck.get(0);
					String target = vector.get(0);
					map.put(src, target);

					m.remove(i);
					reemplazarEstados(m, src, target);
					break;
				}
			}
		}
	}

	/**
	 * Remplaza del automata finito {m} las transiciones a {src} por {target}
	 * 
	 * @param m
	 *            AF
	 * @param src
	 *            estado a reemplazar
	 * @param target
	 *            transicion que reemplaza
	 */
	private static void reemplazarEstados(Vector<Vector<String>> m, String src, String target) {
		for (Vector<String> vector : m) {
			for (int i = 1; i < vector.size() - 1; i++) {
				if (src.equals(vector.get(i))) {
					vector.set(i, target);
				}
			}
		}
	}

	/**
	 * Identifica si un estado {v1} es equivalente al estado {v2}
	 * 
	 * @param v1
	 *            estado 1
	 * @param v2
	 *            estado 2
	 * @return {@link Boolean}
	 */
	private static boolean isEquals(Vector<String> v1, Vector<String> v2) {
		int lastPos = v1.size() - 1;
		if (v1 == null || v1.isEmpty() || v2 == null || v2.isEmpty() || v1.size() != v2.size()) {
			return false;
		}
		for (int i = 1; i <= lastPos; i++) {
			if (!String.valueOf(v1.get(i)).equals(String.valueOf(v2.get(i)))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Restablece los automatas 1 y 2 con los simbolos ingresados
	 * 
	 * @param tbSimbolos
	 *            simbolos de entrada
	 * @param p1
	 *            Automata 1
	 * @param p2
	 *            Automata 2
	 */
	public static void reiniciarAutomatas(JTable tbSimbolos, PanelAF p1, PanelAF p2) {
		List<String> simbolos = getSimbolos(tbSimbolos);
		p1.reset(simbolos);
		p2.reset(simbolos);
		listaEstados.clear();
	}

	/**
	 * Remueve de la tabla los simbolos seleccionados
	 * 
	 * @param tabla
	 *            {@link JTable} Tabla con los simbolos seleccionados
	 */
	public static void removerFila(final JTable tabla) {
		int rows = tabla.getSelectedRows().length;
		String format = String.format("Esta seguro de eliminar %d estado(s)", rows);
		if (rows > 0 && JOptionPane.showConfirmDialog(tabla, format) == JOptionPane.YES_OPTION) {
			for (int i = 0; i < rows; i++) {
				((DefaultTableModel) tabla.getModel()).removeRow(tabla.getSelectedRow());
			}
			tabla.clearSelection();
		}
	}

	/**
	 * Identifica los estados alcanzables de para un AF segun la lista de
	 * conocidos
	 * 
	 * @param conocidos
	 *            lista de estaodos alcanzables inicial
	 * @param m
	 *            Automata
	 * @param remove
	 */
	public static void estadosAlcanzables(Set<String> conocidos, Map<String, Vector<String>> m, boolean remove) {
		// Search reacheable states
		int cuentaIni = conocidos.size();
		Set<String> listTemp = new LinkedHashSet<String>();
		for (String estado : conocidos) {
			Vector<String> vector = m.get(estado);
			if (vector != null) {
				listTemp.addAll(vector.subList(1, vector.size() - 1));
			}
		}
		conocidos.addAll(listTemp);

		if (cuentaIni < conocidos.size()) {
			estadosAlcanzables(conocidos, m, remove);
		}
	}

	/**
	 * Obtiene de un {PanelAF} un {@link Map} con la representacion de un
	 * Automata Finito
	 * 
	 * @param panel
	 * @return
	 */
	public static Map<String, Vector<String>> toMap(PanelAF panel) {
		Vector<Vector<String>> dataVector = ((DefaultTableModel) panel.getTable().getModel()).getDataVector();
		Map<String, Vector<String>> map = new TreeMap<String, Vector<String>>();
		for (Vector<String> vector : dataVector) {
			map.put(vector.get(0), vector);
		}
		return map;
	}

	/**
	 * Adiciona una nueva linea con la fecha actual y el complento adicionado
	 * para el log
	 * 
	 * @param sb
	 *            buffer para adicionar la nueva linea
	 * @param comp
	 *            mensaje a adicionar en la nueva linea
	 */
	static void addLineDate(StringBuffer sb, String comp) {
		sb.append("\n[" + formatter.format(new Date()) + "]- " + comp);
	}

	/**
	 * Verifica si una trama es valida o no segun el automata seleccionado y el
	 * estado inicial
	 * 
	 * @param tpBuffer
	 *            {@link JTextPane} espacio para escribir la respuesta
	 * @param tfTrama
	 *            campo de texto que contiene la trama
	 * @param inicio
	 *            Estado inicial
	 * @param headers
	 *            nombre de los simbolos de entrada
	 * @param af
	 *            representacion del automata finito
	 */
	public static void verificarTrama(JTextPane tpBuffer, JTextField tfTrama, String inicio, Vector<String> headers,
			Map<String, Vector<String>> af) {
		LinkedHashSet<String> conocidos = new LinkedHashSet<String>();
		conocidos.add(inicio);

		StringBuffer sb = new StringBuffer();
		sb.append(tpBuffer.getText());
		addLineDate(sb, "Estado inicial {0}: " + conocidos);

		Util.estadosAlcanzables(conocidos, af, false);
		addLineDate(sb, "Estados conocidos {1}: " + conocidos);

		String trama = tfTrama.getText();

		if (trama != null && !trama.isEmpty()) {
			printCheck(sb, trama, headers, af, conocidos, inicio);
		} else {
			addLineDate(sb, String.format("La trama \"%s\" no es valida", trama));
		}

		tpBuffer.setText(sb.toString());
	}

	/**
	 * Adiciona en el log de operacion la comprobacion de la trama entregada
	 * 
	 * @param sb
	 *            buffer para la escritura
	 * @param trama
	 *            hilera a verificar
	 * @param headers
	 *            nombres de los simbolos de entrada
	 * @param af
	 *            representacion de un atumata finito
	 * @param conocidos
	 *            estados alcanzables
	 * @param inicio
	 *            estado inicial
	 */
	private static void printCheck(StringBuffer sb, String trama, Vector<String> headers,
			Map<String, Vector<String>> af, LinkedHashSet<String> conocidos, String inicio) {
		String estadoActual = inicio;
		Vector<String> vector;
		String format;
		for (char c : trama.toCharArray()) {
			String item = Character.toString(c);
			int pos = headers.indexOf(item);
			if (pos == -1) {
				format = "El item \"%s\" no encontrado";
				addLineDate(sb, String.format(format, item));
				format = "\n >> :. Trama \"%s\" INVALIDA por item \"%s\" DESCONOCIDO, \n - Ultimo estado: \"%s\"\n";
				addLineDate(sb, String.format(format, trama, item, estadoActual));
				return;
			}
			vector = af.get(estadoActual);
			if (vector == null) {
				format = "El estado \"%s\" no encontrado";
				addLineDate(sb, String.format(format, estadoActual));
				format = "\n >> :. Trama \"%s\" INVALIDA por Estado \"%s\" DESCONOCIDO, \n - Ultimo estado: \"%s\"\n";
				addLineDate(sb, String.format(format, trama, estadoActual, estadoActual));
				return;
			}
			estadoActual = vector.get(pos);
			addLineDate(sb, String.format("El item \"%s\" a estado \"%s\"", item, estadoActual));
		}

		vector = af.get(estadoActual);
		if (vector == null) {
			format = "\n >> :. Trama \"%s\" es INVALIDA por estado DESCONOCIDO, \n - Ultimo estado: \"%s\"\n";
		} else {
			Object aceptado = vector.get(vector.size() - 1);
			if (String.valueOf(false).equals(aceptado)) {
				format = "\n >> :. Trama \"%s\" es INVALIDA de NO ACEPTACION, \n - Ultimo estado: \"%s\"\n";
			} else {
				format = "\n >> :. La trama \"%s\" es VALIDA, \n * Ultimo estado: \"%s\"\n";
			}
		}
		addLineDate(sb, String.format(format, trama, estadoActual));

	}

	/**
	 * Escribe en consola un mensaje de error
	 * 
	 * @param msg
	 */
	public static void printErr(String msg) {
//		System.err.println(msg);
	}

	/**
	 * Enumeracion de imagenes de la ruta
	 * 
	 * @author Fredy
	 *
	 */
	static enum IconsEnum {
		ADD("add-32.png"), ADD_GREEN("add-16-green.png"), DEL("delete-16-13.png"), CHECK("checked-16.png"), ERASER(
				"eraser-16.png"), ACS("access-16.png"), CFG("configure-32.png"), SIMPLIFY("simplify-24.png"), SUCCESS(
						"success-16.png"), REM("remove-16.png"), OP_U("union-32.png"), OP_I(
								"intersection-32.png"), ONE("fingers-one-26.png"), TWO("fingers-two-26.png");

		IconsEnum(String valor) {
			this.valor = valor;
		}

		String valor;
	}
}
