package edu.udea.control;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import edu.udea.control.Util.IconsEnum;

/**
 * Panel que contiene la representacion grafica para visializar y operar un
 * aut&oacute;ata finito
 * 
 * @author Fredy
 *
 */
@SuppressWarnings({"unchecked"})
public class PanelAF extends JPanel {

	private static final String HEADER_ESTADOS = "Estados";
	private static final String HEADER_ACEPT = "Aceptaci\u00F3n";
	private static final long serialVersionUID = 1L;
	private boolean editable = true;

	private JTable table = new JTable() {
		private static final long serialVersionUID = 1L;

		public boolean isCellEditable(int row, int column) {
			boolean b = column == 0 ? false : !isEditable() ? false : super.isCellEditable(row, column);
			return b;
		}
	};
	private DefaultTableCellRenderer centerRenderer;
	private JComboBox<String> cbEstadoInicial = new JComboBox<String>();
	private JPanel pCommand;
	private JButton btnAdd;
	private JButton btnRemove;

	/**
	 * Create the panel.
	 */
	public PanelAF(String titulo) {
		buildPanel(titulo);
	}

	private void buildPanel(String titulo) {
		setBorder(new TitledBorder(null, titulo, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setLayout(new BorderLayout(0, 0));

		pCommand = new JPanel();
		pCommand.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(pCommand, BorderLayout.NORTH);

		cbEstadoInicial.setVisible(false);
		pCommand.add(cbEstadoInicial);

		btnAdd = new JButton(Util.getIcon(IconsEnum.ADD_GREEN));
		btnAdd.setToolTipText("Adicionar Estado");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Util.addEstado(getTable());
			}
		});
		pCommand.add(btnAdd);

		btnRemove = new JButton(Util.getIcon(IconsEnum.DEL));
		btnRemove.setToolTipText("Eliminar estado seleccionado");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Util.removerFila(getTable());
			}
		});
		pCommand.add(btnRemove);

		JScrollPane scrollPaneContent = new JScrollPane();
		add(scrollPaneContent, BorderLayout.CENTER);

		table.setModel(new DefaultTableModel(new Vector<String>(Arrays.asList(HEADER_ESTADOS, HEADER_ACEPT)), 0));
		centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(String.class, centerRenderer);
		table.getColumnModel().getColumn(0).setPreferredWidth(145);

		table.setSurrendersFocusOnKeystroke(true);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.setCellSelectionEnabled(true);
		table.setColumnSelectionAllowed(true);
		scrollPaneContent.setViewportView(table);
		Util.setIconHeader(table);
		Util.setFirstCellStyle(table);
	}

	public JTable getTable() {
		return table;
	}

	/**
	 * Restablece el automata con los simbolos entregados
	 * 
	 * @param simbolos
	 */
	public void reset(List<String> simbolos) {
		Vector<String> headers = new Vector<String>(simbolos);
		headers.add(0, HEADER_ESTADOS);
		headers.add(HEADER_ACEPT);
		table.setModel(new DefaultTableModel(headers, 0));
		table.setDefaultRenderer(String.class, centerRenderer);
		Util.setIconHeader(table);
		Util.setFirstCellStyle(table);
	}

	/**
	 * Restablece el automata con los simbolos entregados
	 * 
	 * @param simbolos
	 */
	public void render(Vector<String> columnas, Vector<Vector<String>> datos) {
		table.setModel(new DefaultTableModel(datos, columnas));
		table.setDefaultRenderer(String.class, centerRenderer);
		Util.setIconHeader(table);
		Util.setFirstCellStyle(table);
	}

	public Vector<Vector<String>> getDatos() {
		Vector<Vector<String>> dataVector = ((DefaultTableModel) table.getModel()).getDataVector();
		return dataVector;
	}

	public Vector<String> getColumnas() {
		Vector<String> data = new Vector<String>();
		for (int i = 0; i < table.getModel().getColumnCount(); i++) {
			data.addElement(table.getModel().getColumnName(i));
		}
		return data;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
		pCommand.setVisible(editable);
		btnAdd.setVisible(editable);
		btnRemove.setVisible(editable);
		cbEstadoInicial.setVisible(editable);
	}

	/**
	 * Establece el estado inicial para el automata finito
	 * 
	 * @param estadosPosibles
	 */
	public void setEstadoInical(Vector<String> estadosPosibles) {
		cbEstadoInicial.removeAllItems();
		for (String estado : estadosPosibles) {
			cbEstadoInicial.addItem(estado);
		}

		if (cbEstadoInicial.getItemCount() > 0) {
			cbEstadoInicial.setSelectedIndex(0);
			cbEstadoInicial.setVisible(true);
		} else {
			cbEstadoInicial.setVisible(false);
		}
	}

	/**
	 * Obtiene el estado inicial seleccionado de la pantalla de usuario
	 */
	public String getEstadoInicial() {
		String item = (cbEstadoInicial != null) ? (String) cbEstadoInicial.getSelectedItem() : null;
		return item;
	}

}
