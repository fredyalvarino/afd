package edu.udea.control;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import edu.udea.control.Util.IconsEnum;

/**
 * Panel de acciones, contiene los componentes visuales para realizar las
 * operaciones de adicionar estados a los automatas 1 y 2, restablecer los
 * automatas con los simbolos de entrada, efectuar la union y la interseccion de
 * los automatas
 * 
 * @author Fredy Alvarino
 *
 */
public class PanelAcciones extends JPanel {

	private static final long serialVersionUID = 1L;
	private JButton btnUnion;
	private JButton btnInterseccion;
	private JButton btnRestablecer;
	private JButton btnEjemplo1;
	private JButton btnEjemplo2;

	/**
	 * Creacion del panel de acciones.
	 */
	public PanelAcciones(String titulo) {
		setBorder(new TitledBorder(null, titulo, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		btnRestablecer = getButton("Restablecer Aut\u00F3matas", IconsEnum.CFG);
		btnUnion = getButton("Unir Aut\u00F3matas", IconsEnum.OP_U);
		btnInterseccion = getButton("Intersecci\u00F3n", IconsEnum.OP_I);

		add(new JSeparator());

		btnEjemplo1 = getButton("Ej1: CerosPares y UnosImpares", IconsEnum.ONE);
		btnEjemplo2 = getButton("Ej2: Inicia y/o termina en 01", IconsEnum.TWO);
	}

	/**
	 * Construye y entrega un boton con las especificaciones dadas de texto e
	 * imagen
	 * 
	 * @param tooltip
	 * @param image
	 * @return
	 */
	private JButton getButton(String tooltip, IconsEnum image) {
		JButton boton = new JButton(Util.getIcon(image));
		boton.setToolTipText(tooltip);
		add(boton);
		return boton;
	}

	/**
	 * Establece la configuracion para los eventos generales de la aplicacion
	 * 
	 * @param tbEstados
	 *            tabla de estados
	 * @param panelAF_1
	 *            Automata 1
	 * @param panelAF_2
	 *            Automata 2
	 * @param panelAF_RES
	 *            Automata de respuesta
	 * @param panelTest
	 *            panel de validacion y verificacion.
	 */
	public void configurar(final JTable tbEstados, final PanelAF panelAF_1, final PanelAF panelAF_2,
			final PanelAF panelAF_RES, final PanelTest panelTest) {
		Util.reiniciarAutomatas(tbEstados, panelAF_1, panelAF_2);

		btnRestablecer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Util.reiniciarAutomatas(tbEstados, panelAF_1, panelAF_2);
				panelTest.setEstados();
			}
		});
		btnUnion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Map<String, String> equivalentes = Util.unirAFD(panelAF_1, panelAF_2, panelAF_RES, true);
				panelTest.setEstados();
				panelTest.showMsg("Estados equivalentes: " + equivalentes + "\n");
			}
		});
		btnInterseccion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Map<String, String> equivalentes = Util.unirAFD(panelAF_1, panelAF_2, panelAF_RES, false);
				panelTest.setEstados();
				panelTest.showMsg("Estados equivalentes: " + equivalentes + "\n");
			}
		});

		btnEjemplo1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Vector<String> columnas = new Vector<String>(Arrays.asList("", "0", "1", ""));

				Vector<Vector<String>> datos;
				datos = new Vector<Vector<String>>();
				datos.add(new Vector<String>(Arrays.asList("CP", "CI", "CP", "true")));
				datos.add(new Vector<String>(Arrays.asList("CI", "CP", "CI", "false")));

				panelAF_1.render(columnas, datos);
				panelAF_1.setEstadoInical(new Vector<String>(Arrays.asList("CP", "CI")));

				datos = new Vector<Vector<String>>();
				datos.add(new Vector<String>(Arrays.asList("UP", "UP", "UI", "false")));
				datos.add(new Vector<String>(Arrays.asList("UI", "UI", "UP", "true")));
				panelAF_2.render(columnas, datos);
				panelAF_2.setEstadoInical(new Vector<String>(Arrays.asList("UP", "UI")));
			}
		});
		btnEjemplo2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Secuencia que termina en 01
				Vector<String> columnas = new Vector<String>(Arrays.asList("", "0", "1", ""));

				Vector<Vector<String>> datos;
				datos = new Vector<Vector<String>>();
				datos.add(new Vector<String>(Arrays.asList("NA", "UC", "NA", "false")));
				datos.add(new Vector<String>(Arrays.asList("UC", "UC", "CU", "false")));
				datos.add(new Vector<String>(Arrays.asList("CU", "UC", "NA", "true")));

				panelAF_2.render(columnas, datos);
				panelAF_2.setEstadoInical(new Vector<String>(Arrays.asList("NA")));

				// Secuencia que inicia con 01
				datos = new Vector<Vector<String>>();
				datos.add(new Vector<String>(Arrays.asList("INI", "UNC", "ERR", "false")));
				datos.add(new Vector<String>(Arrays.asList("UNC", "ERR", "EXI", "false")));
				datos.add(new Vector<String>(Arrays.asList("ERR", "ERR", "ERR", "false")));
				datos.add(new Vector<String>(Arrays.asList("EXI", "EXI", "EXI", "true")));
				panelAF_1.render(columnas, datos);
				panelAF_1.setEstadoInical(new Vector<String>(Arrays.asList("INI")));
			}
		});
	}

}
